/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenextempo;

/**
 *
 * @author marco
 */
public class Extempo {
    private int tipo;
    private String nombre;
    private String fecha;
    private String domicilio;
    private float costo;
    private int kilowatts;
    private int numRecibo;
    
    public Extempo(int numRecibo, int tipo, String nombre, String fecha, String domicilio, float costo, int kilowatts) {
        this.numRecibo = numRecibo;
        this.tipo = tipo;
        this.nombre = nombre;
        this.fecha = fecha;
        this.domicilio = domicilio;
        this.costo = costo;
        this.kilowatts = kilowatts;
    }
    
    public Extempo() {
        this.numRecibo = 0;
        this.tipo = 0;
        this.nombre = "";
        this.fecha = "";
        this.domicilio = "";
        this.costo = 0.0f;
        this.kilowatts = 0;
    }
       
    public Extempo(Extempo x) {
        this.numRecibo = x.numRecibo;
        this.tipo = x.tipo;
        this.nombre = x.nombre;
        this.fecha = x.fecha;
        this.domicilio = x.domicilio;
        this.costo = x.costo;
        this.kilowatts = x.kilowatts;
    }
    
    public int getNumRecibo() {
        return numRecibo;
    }
    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public int getKilowatts() {
        return kilowatts;
    }

    public void setKilowatts(int kilowatts) {
        this.kilowatts = kilowatts;
    }

    
     public float calcularSubTotal(){
        float subtotal = 0.0f;
        switch(this.tipo){
            case 1: 
                this.costo = 2.0f;
                break;
            case 2:
                this.costo = 3.0f;
                break;
            case 3:
            this.costo = 5.0f;
                break;
        }
        subtotal = this.kilowatts * this.costo;
        return subtotal;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularSubTotal() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = this.calcularSubTotal() + this.calcularImpuesto();
        return total;
    }
    
}
 
